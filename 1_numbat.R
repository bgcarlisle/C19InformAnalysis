library(tidyverse)

## Make a folder to contain downloaded data
if ( ! file.exists("data/")) {
    dir.create("data/")
}

## Make a folder to contain downloaded data
if ( ! file.exists("working/")) {
    dir.create("working/")
}

## Make a folder to contain processed output
if ( ! file.exists("output/")) {
    dir.create("output/")
}

## Make a folder to contain plots
if ( ! file.exists("plots/")) {
    dir.create("plots/")
}

## Download the main extraction tables

ext1 <- read_tsv("https://numbat.bgcarlisle.com/c19-informativeness/export/2021-02-09_092029-form_1-refset_16-final.tsv")

ext2 <- read_tsv("https://numbat.bgcarlisle.com/c19-informativeness/export/2021-02-09_092046-form_1-refset_17-final.tsv")

extractions <- bind_rows(
    ext1,
    ext2
)

## Update statuses to their values at 6 months after launch
updates <- read_csv("data/status_updates.csv")

extractions <- extractions %>%
    left_join(updates)

extractions$overall_status <- ifelse(
    ! is.na(extractions$status_at_6_mo),
    extractions$status_at_6_mo,
    extractions$overall_status
)

extractions <- extractions %>%
    mutate(status_at_download = NULL) %>%
    mutate(status_at_6_mo = NULL)

## Update blinding to their values at 6 months after launch
blindingupdates <- read_csv("data/blinding_updates.csv")

extractions <- extractions %>%
    left_join(blindingupdates)

extractions$blinding <- ifelse(
    ! is.na(extractions$blinding_at_6_mo),
    extractions$blinding_at_6_mo,
    extractions$blinding
)

extractions <- extractions %>%
    mutate(blinding_at_download = NULL) %>%
    mutate(blinding_at_6_mo = NULL)

## Write to disk
extractions %>%
    write_csv("data/extractions.csv")

## Download the arms data

arm1 <- read_tsv("https://numbat.bgcarlisle.com/c19-informativeness/export/2021-02-09_092227-table_arms-refset_16-table-final.tsv")

arm2 <- read_tsv("https://numbat.bgcarlisle.com/c19-informativeness/export/2021-02-09_092259-table_arms-refset_17-table-final.tsv")

bind_rows(
    arm1,
    arm2
) %>%
    write_csv("data/arms.csv")

## Download the primary outcomes data

po1 <- read_tsv("https://numbat.bgcarlisle.com/c19-informativeness/export/2021-02-09_092355-table_primary-refset_16-table-final.tsv")

po2 <- read_tsv("https://numbat.bgcarlisle.com/c19-informativeness/export/2021-02-09_092419-table_primary-refset_17-table-final.tsv")

bind_rows(
    po1,
    po2
) %>%
    write_csv("data/primaryoutcomes.csv")

## Download the locations data

loc1 <- read_tsv("https://numbat.bgcarlisle.com/c19-informativeness/export/2021-02-09_094053-table_locations-refset_16-table-final.tsv")

loc2 <- read_tsv("https://numbat.bgcarlisle.com/c19-informativeness/export/2021-02-09_092548-table_locations-refset_17-table-final.tsv")

bind_rows(
    loc1,
    loc2
) %>%
    write_csv("data/locations.csv")

