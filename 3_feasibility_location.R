library(tidyverse)
library(ggplot2)
library(car)

loc <- read_csv("data/locations.csv")

## Filter excluded
extractions <- read_csv("data/extractions.csv") %>%
    filter(include == 1) %>%
    filter(tx_prev_treatment == 1)
loc$include <- loc$nctid %in% extractions$nctid
loc <- loc %>%
    filter(include)

## Read in whether a trial is non-feasible based on the output from
## 2_feasibility_enrolment.R
nf <- read_csv("output/non-feasible.csv")
nf$nonfeasible <- TRUE
nf <- nf %>%
    select(nctid, nonfeasible)

trial_has_at_least_one_us_location <- function (nct) {

    trial_us_locations <- loc %>%
        filter(nctid == nct) %>%
        filter(auto_country == "United States")

    nrow(trial_us_locations) > 0 %>% return()
    
}

loc$trial_us_loc <- sapply(
    loc$nctid,
    trial_has_at_least_one_us_location
)

## Double-check: These NCT's should correspond to
## trials that have at least one US location and
## at least one non-US location
loc %>%
    filter(auto_country != "United States") %>%
    filter(trial_us_loc) %>%
    group_by(nctid) %>%
    slice_head() %>%
    select(nctid)

## Make a new data frame in which we are only looking
## at trials with at least one US location
loc_us <- loc %>%
    filter(trial_us_loc) %>%
    mutate(trial_us_loc = NULL)

## Get the treatment and prevention columns from the
## extractions CSV
extr_tx_prev <- read_csv("working/extractions-hist.csv") %>%
    select(nctid, tx_prev_treatment, tx_prev_prevention) %>%
    mutate(tx = tx_prev_treatment == 1) %>%
    mutate(tx_prev_treatment = NULL) %>%
    mutate(prev = tx_prev_prevention == 1) %>%
    mutate(tx_prev_prevention = NULL)

## Get the first estimated trial enrolment
extr_enrol <- read_csv("working/extractions-hist.csv") %>%
    select(nctid, `1st_enrol`, original_start_date, original_pcdate)    

## Add those to the end of the locations data frame
loc_us <- loc_us %>%
    mutate(`1st_enrol` = NULL) %>%
    left_join(extr_tx_prev) %>%
    left_join(extr_enrol) %>%
    rename(trial_enrol = `1st_enrol`)

## Add the non-feasible column
loc_us <- loc_us %>%
    left_join(nf)

txprev_factor <- function(loctx, locprev) {

    factor <- 0

    if (loctx & ! locprev) {
        factor <- 1
    }

    if (loctx & locprev) {
        factor <- 0.5
    }

    return(factor)
    
}

loc_us$txprev_factor <- mapply(
    txprev_factor,
    loc_us$tx,
    loc_us$prev
)

trial_loc_n <- function(nct) {
    
    loc %>%
        filter(nctid == nct) %>%
        nrow() %>%
        return()
    
}

loc_us$trial_loc_n <- sapply(
    loc_us$nctid,
    trial_loc_n
)

## Double check, this should give you the number of locations per
## trial, whether they're in the US or not
loc_us %>%
    select(nctid, trial_loc_n)

## Calculate the percent of the enrolment time that comes before
## the cutoff date, 2020 July 1
percent_enrol_before_july <- function (start, completion) {

    cutoff_date <- as.Date("2020-07-01")

    if ( completion < cutoff_date ) {
        percent <- 1
    } else {
        days_before <- cutoff_date - start

        percent <- days_before / as.numeric(completion - start)
    }

    return (percent)
    
}

loc_us$percent_enrol_before_july <- mapply(
    percent_enrol_before_july,
    loc_us$original_start_date,
    loc_us$original_pcdate
)

## This combines all the factors we calculated above into the
## enrolment estimate for each location
loc_us$loc_enrol <- as.numeric(loc_us$auto_country == "United States") * loc_us$trial_enrol * loc_us$txprev_factor * loc_us$percent_enrol_before_july / loc_us$trial_loc_n

states <- loc_us %>%
    filter(auto_country == "United States") %>%
    group_by(auto_state) %>%
    slice_head() %>%
    select(auto_state) %>%
    rename(state = auto_state)

statenames <- read_csv("data/statenames.csv") %>%
    rename(state = State) %>%
    rename(code = Code)

states <- states %>%
    left_join(statenames)

get_state_target_enrolment <- function(state) {
    loc_state <- loc_us %>%
        filter(auto_country == "United States") %>%
        filter(auto_state == state)

    loc_state$loc_enrol %>%
        sum() %>%
        return()
}

states$target_enrol <- sapply(
    states$state,
    get_state_target_enrolment
)

states %>%
    arrange(desc(target_enrol))

usafacts <- read_csv("https://static.usafacts.org/public/data/covid-19/covid_confirmed_usafacts.csv")

get_cases_for_state <- function(state) {

    statecases <- usafacts %>%
        filter(State == state) %>%
        select(`2020-06-30`)

    statecases$`2020-06-30` %>%
        sum() %>%
        return()
    
}

states$cases <- sapply(
    states$code,
    get_cases_for_state
)

states$prop_goalenrol_to_cases <- states$target_enrol / states$cases

## Get number of locations in a state that are in a trial that is
## non-feasible as defined in 2_feasibility_enrolment.R and divide
## that by the number of trial locations in that state
nf_by_state <- loc_us %>%
    filter(auto_country == "United States") %>%
    group_by(auto_state) %>%
    mutate(nf_prop = sum(nonfeasible, na.rm=TRUE)/n()) %>%
    slice_head() %>%
    rename(state = auto_state) %>%
    select(state, nf_prop)

## Join to states dataframe
states <- states %>%
    left_join(nf_by_state)

states %>%
    arrange(desc(prop_goalenrol_to_cases)) %>%
    select(state, target_enrol, cases, prop_goalenrol_to_cases, nf_prop)

## In response to reviewer comments, we removed states that have only
## 1 trial in them
states <- states %>%
    filter(code != "KS") %>%
    filter(code != "ND") %>%
    filter(code != "VT")

## Plot
feasibility_model_plot <- ggplot(
    aes(
        x = prop_goalenrol_to_cases,
        y = nf_prop,
        label = code
    ),
    data = states
) +
    geom_point() +
    geom_text(
        nudge_x=0.0005,
        nudge_y=0.01
    ) +
    labs(
        x = "Proportion of cases in state required to reach full goal enrolment",
        y = "Proportion of trial locations in a state that are part of a trial with unsuccessful recruitment"
    )+
    geom_smooth(method='lm') +
    scale_x_continuous(labels = scales::percent) +
    scale_y_continuous(labels = scales::percent)

pdf(
    "plots/feasibility_by_state.pdf",
    width=9,
    height=7
)
feasibility_model_plot
dev.off()

png(
    "plots/feasibility_by_state.png",
    width=9,
    height=6,
    units="in",
    res=300
)
feasibility_model_plot
dev.off()

## Write to disc
states %>%
    write_csv("output/feasibility_by_state.csv")
