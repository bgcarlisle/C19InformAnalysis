library(tidyverse)

extractions <- read_csv("working/extractions-hist.csv")

## Remove trials where location of care or severity data could
## not be extracted
r.extr <- extractions %>%
    filter(location_of_care_ns != 1) %>%
    filter(severity_ns != 1)

## How many trials can be included in this analysis?
red_incl_k <- r.extr %>% nrow()

## Get age data

r.extr$age_split <- strsplit(r.extr$age, " to ")

r.extr$age_l <- sapply(r.extr$age_split, function(x) x[1])

r.extr$age_u <- sapply(r.extr$age_split, function(x) x[2])

r.extr$age_lower <- str_extract(r.extr$age_l, "[0-9]+")

r.extr$age_upper <- str_extract(r.extr$age_u, "[0-9]+")

r.extr$includes_elderly <- is.na(r.extr$age_upper) | as.numeric(r.extr$age_upper) >= 60

## Add a column that includes phase, location of care, age, severity and tx/prev
r.extr$redundancy <- paste(
    r.extr$phase,
    r.extr$location_of_care_ambulatory == 1,
    r.extr$location_of_care_hospitalized == 1,
    r.extr$location_of_care_intensive_care == 1,
    r.extr$includes_elderly,
    r.extr$severity_asymptomatic == 1,
    r.extr$severity_mild == 1,
    r.extr$severity_moderate == 1,
    r.extr$severity_severe == 1,
    r.extr$severity_critical == 1,
    r.extr$severity_healthy == 1,
    r.extr$tx_prev_treatment == 1,
    r.extr$tx_prev_prevention == 1
)

## Sort by start date at 6 months
r.extr <- r.extr %>%
    arrange(start_date_6mo)

## Flag potential duplicates
r.extr$dup <- r.extr$redundancy %>%
    duplicated()

## Mark which other trials share the same location, age and severity
other_trials_with_same_redundancy_column_value <- function(nct, red) {

    samered <- r.extr %>%
        filter(nctid != nct) %>%
        filter(redundancy == red)
    
    samered$nctid %>%
        paste(collapse=" ") %>%
        return()
    
}

r.extr$ncts_with_same_red <- mapply(
    other_trials_with_same_redundancy_column_value,
    r.extr$nctid,
    r.extr$redundancy
)

## Write to file for use in 7_duplicates.R
r.extr %>%
    select(nctid, dup, ncts_with_same_red, start_date_6mo) %>%
    write_csv("working/potential-dup-trials.csv")

## Do the post hoc "lower bar" analysis for redundancy
sum(r.extr$dup) / red_incl_k
