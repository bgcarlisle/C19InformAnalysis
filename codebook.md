---
title: "Extraction of Covid-19 trials"
date: "2021-02-15"
---

# Extraction form elements

## Extraction timer

Numbat automatically times all extractions starting from the first time a user opens the extraction, until the first time they click 'Complete'. This element displays a timer to the user when the extraction is on-going, and allows the user to re-start the timer.

### NCT Number

Extractors were prompted with the following. Names of columns in the uploaded reference set preceded by '$' would be replaced with the value of the corresponding column from the row of the reference to be extracted.

```
<a href="https://clinicaltrials.gov/ct2/show/$nctid" target="_blank">$nctid</a>

```

### 

Extractors were prompted with the following. Names of columns in the uploaded reference set preceded by '$' would be replaced with the value of the corresponding column from the row of the reference to be extracted.

```
$check
```

### Phase

Extractors were prompted with the following. Names of columns in the uploaded reference set preceded by '$' would be replaced with the value of the corresponding column from the row of the reference to be extracted.

```
$phase
```

### Intervention types in this trial

Extractors were prompted with the following. Names of columns in the uploaded reference set preceded by '$' would be replaced with the value of the corresponding column from the row of the reference to be extracted.

```
$interventions
```

### Include or exclude

Variable type: Categorical (single selection only)

Database column name: `include`

Extractors were prompted to select one of the following mutually exclusive options.

| Displayed option name | Database value |
|:----------------------|---------------:|
| Include | 1 |
| Exclude | 0 |

Extractor prompt: 
```
Inclusion criteria:
    • Interventional clinical trials
    • Include: drugs, biologicals, surgery, radiation, devices
    • Trial Status: Completed, Terminated, Suspended, Active not recruiting, Enrolling by invitation, Recruiting
    • Trials testing an efficacy hypothesis as a primary outcome
    • Phase numbers: Phase 1/2, Phase 2, Phase 2/3, Phase 3

Exclusion criteria:
    • Behavioural interventions (including psychological, dietary, physiotherapy, etc)
    • Trials of natural products (not regulated by the FDA)
    • Phase 1 trials (do not typically evaluate efficacy outcomes and are not required by FDA to register)

Include trials of a natural products in combination with or versus a drug/biological/device intervention. 
Exclude trials in which Traditional Chinese Medicine is a single regiment, but include those one in which Traditional Chinese Medicine is given in combination with another drug. 

Watch out for compounds such as melatonin or lactoferrin (natural products).
Look at the trial scope - watch out for epidemiology or pharmacokinetics trials.
```

### Treatment or prevention

Variable type: Categorical (multiple selection allowed)

Extractors were prompted to select one or more of the following options.

The options selected by extractors would be exported with a 1 in the corresponding database column.

| Displayed option name | Database column |
|:----------------------|----------------:|
| Treatment | tx_prev_treatment |
| Prevention | tx_prev_prevention |
| Unclear or not stated | tx_prev_ns |

Extractor prompt: 
```
Coronavirus prevention trial: A clinical trial conducted in a population without known  SARS-CoV-2 infection designed to evaluate the safety and/or efficacy of an intervention directed at preventing serologic conversion and/or clinical disease due to SARS-CoV-2. These studies are often conducted in an at-risk population, where-by exposure to SARS-CoV-2 is felt to be likely. They can include populations who have been exposed to SARS-CoV-2 (post-exposure prophylaxis), without documented infection or clinical symptoms of disease, in addition to populations who may be exposed to SARS-CoV-2 at a future time (pre-exposure prophylaxis).
```

## Non-redundancy

### Location of care

Variable type: Categorical (multiple selection allowed)

Extractors were prompted to select one or more of the following options.

The options selected by extractors would be exported with a 1 in the corresponding database column.

| Displayed option name | Database column |
|:----------------------|----------------:|
| Ambulatory | location_of_care_ambulatory |
| Hospitalized | location_of_care_hospitalized |
| Intensive care | location_of_care_intensive_care |
| Unclear or not stated | location_of_care_ns |

Extractor prompt: 
```
If something in the registry would imply hospitalization, mark as hospitalized — eg. in case of treatment trials - IV infusion may indicate hospitalization (route of administration as an auxiliary factor/indicator in the assessment). 

Patient with severe and critical disease - usually hospitalized. 

Patient with critical disease - mark also as intensive care, unless the test description says otherwise (NOTE for us: this may be a limitation of our study).
```

### Severity

Variable type: Categorical (multiple selection allowed)

Extractors were prompted to select one or more of the following options.

The options selected by extractors would be exported with a 1 in the corresponding database column.

| Displayed option name | Database column |
|:----------------------|----------------:|
| Healthy | severity_healthy |
| Asymptomatic | severity_asymptomatic |
| Mild | severity_mild |
| Moderate | severity_moderate |
| Severe | severity_severe |
| Critical | severity_critical |
| Unclear or not stated | severity_ns |

Extractor prompt: 
```
As described by the investigators.

If not provided by investigators, apply <a href="https://www.who.int/publications/i/item/clinical-management-of-covid-19" target="_blank">WHO criteria</a> (see page 13) to title, description, primary/secondary outcomes and inclusion criteria

If inferring level of severity (not stated directly by investigators), mark all possible levels of severity (based mainly on inclusion/exclusion criteria) BUT in case of hospitalized patient - do not use the category of mild disease (unless it is directly indicated in the record or inclusion/exclusion criteria).

Study with healthy patients - mark as health and asymptomatic (if healthy patients were not tested for COVID-19 prior to enrollment - look at inclusion/exclusion criteria).

When determining the severity of the disease, pay attention to the oxygen flow, saturation, and ventilation.

If mechanical ventilation is excluded - the patient is not critical.
```

### Arms

Table data

Extractors were prompted to add rows to a table of open text fields with the following column headings.

| Displayed column name | Database column name |
|:----------------------|---------------------:|
| Drug name | type |
| Category | category |

Extractor prompt: 
```
Drug name: include combinations, do not include "best supportive care".

Collapse different dosing/schedule regimen arms

Category:  <a href="https://www.who.int/docs/default-source/coronaviruse/covid-classification-of-treatment-types-rev.pdf" target="_blank">WHO classification</a> (try to find the category that best fits; don't make up a new one if possible)

Placebo category: leave blank

For standard of care - follow by investigator declaration (in may also include drugs).

```

### Comparator

Variable type: Categorical (single selection only)

Database column name: `comparator_arm`

Extractors were prompted to select one of the following mutually exclusive options.

| Displayed option name | Database value |
|:----------------------|---------------:|
| There is a comparator arm | 1 |
| There is no comparator arm | 0 |
| Unclear or not stated | ns |

## Trial design

### Primary outcomes

Table data

Extractors were prompted to add rows to a table of open text fields with the following column headings.

| Displayed column name | Database column name |
|:----------------------|---------------------:|
| Outcome | outcome |
| Clinical, surrogate (C or S, upper case), positive test (T) or procedural (P) | clinical_or_surrogate |
| Timepoint | timepoint |

Extractor prompt: 
```
The primary outcomes have been auto-extracted from the ClinicalTrials.gov record.

Treatment trial: evaluate clinical vs surrogate for outcomes that affect patient welfare.

If a primary outcome is a composite of clinical and surrogate measures, default to clinical.

Oxygenation index - indicate clinical outcome. 

Use T (indicate SARS-nCov-2 test or anti SARS-COV-2 antibodies) category for prevention trials. 

For prevention trial, if symptoms and clinical outcomes are measured together with COVID-19 laboratory test, write CT (when test is not stated write only C). 

Procedural outcome (P): includes feasibility or dosing.

Timepoint: in case of expression "7 days after randomization" write only 7 days. 

For adverse events - use S. 
```

### Placebo or standard-of-care control

Variable type: Categorical (single selection only)

Database column name: `placebo_control`

Extractors were prompted to select one of the following mutually exclusive options.

| Displayed option name | Database value |
|:----------------------|---------------:|
| An arm of this study represents a placebo or standard-of-care control | 1 |
| No arm of this study represents placebo or standard-of-care control | 0 |
| It is unclear or not stated whether there is a placebo or standard-of-care control | ns |

Extractor prompt: 
```
Placebo may also include standard of care. 

In case of standard of care follow by investigator declaration - without analyzing the definition of a therapeutic scheme which can be used. 

For prevention studies, if they say “no intervention” equate this with standard of care.

```

### Minimum and maximum age from ClinicalTrials.gov

Extractors were prompted with the following. Names of columns in the uploaded reference set preceded by '$' would be replaced with the value of the corresponding column from the row of the reference to be extracted.

```
$age
```

### Elderly patients

Variable type: Categorical (single selection only)

Database column name: `elderly_included`

Extractors were prompted to select one of the following mutually exclusive options.

| Displayed option name | Database value |
|:----------------------|---------------:|
| Elderly patients (60+) are included | 1 |
| Elderly patients are not included | 0 |
| It is unclear or not stated whether elderly patients are included | ns |

### Is this an adaptive trial design or a multi-arm 

Variable type: Categorical (single selection only)

Database column name: `adaptive_or_multi_arm`

Extractors were prompted to select one of the following mutually exclusive options.

| Displayed option name | Database value |
|:----------------------|---------------:|
| Yes, this is an adaptive or multi-arm study | yes |
| No, this is not an adaptive or multi-arm study | no |

### Are there arms of this trial that have closed, or is there anything particular about this trial that should be mentioned?

Variable type: Text area

Database column name: `adaptive_multi_arm_notes`

## Feasibility

### Overall status

Extractors were prompted with the following. Names of columns in the uploaded reference set preceded by '$' would be replaced with the value of the corresponding column from the row of the reference to be extracted.

```
$overall_status
```

### 'Why stopped'

Extractors were prompted with the following. Names of columns in the uploaded reference set preceded by '$' would be replaced with the value of the corresponding column from the row of the reference to be extracted.

```
$why_stopped
```

### Trial was stopped for feasibility reasons

Variable type: Categorical (single selection only)

Database column name: `stopped_feasibility`

Extractors were prompted to select one of the following mutually exclusive options.

| Displayed option name | Database value |
|:----------------------|---------------:|
| Stopped for feasibility issues | 1 |
| Stopped for efficacy, safety, or reasons related to the progress of science | 0 |
| Rationale unclear or not stated | ns |
| Trial has not stopped | ongoing |

Extractor prompt: 
```
The "why stopped?" field contains a rationale that does not indicate efficacy, safety, or reasons related to the progress of science.

Only complete this question if the status of the trial is suspended, terminated or withdrawn.
```

### Locations

Table data

Extractors were prompted to add rows to a table of open text fields with the following column headings.

| Displayed column name | Database column name |
|:----------------------|---------------------:|
| Name | auto_name |
| Country | auto_country |
| State | auto_state |

## Meta

### Extraction notes to be shared with other extractors

Variable type: Text area

Database column name: `why_weird`

# Acknowledgements

This codebook was automatically generated by Numbat Systematic Review Mananger.(1)

# References

1. Carlisle, B. G. Numbat Systematic Review Manager [Software]. Retrieved from https://numbat.bgcarlisle.com: *The Grey Literature*; 2020. Available from: https://numbat.bgcarlisle.com