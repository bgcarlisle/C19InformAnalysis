# C19InformAnalysis

Code for the analysis of "The informativeness of trials in COVID-19: A longitudinal cohort analysis of trials registered on ClinicalTrial.gov"

To start, also clone the following repositories:

* [Clinical Trials History Scraper](https://codeberg.org/bgcarlisle/ClinicalTrialsHistoryScraper "Clinical Trials History Scraper")
* [Drug Synonym Checker](https://codeberg.org/bgcarlisle/DrugSynonymCheck "Drug Synonym Checker")

## `1_numbat.R`

Downloads data from Numbat

Output:

* `data/extractions.csv`, per-trial extracted data
* `data/arms.csv`, per-arm extracted data
* `data/primaryoutcomes.csv`, per-outcome extracted data
* `data/locations.csv`, per-site extracted data

## `2_feasibility_enrolment.R`

Contains instructions for automatically downloading historical data for clinical trials

Calculates original start dates, original and final primary completion dates, original enrolment and latest actual enrolment

Output:

* `data/historical_versions.csv`, per-historical version clinical trial data
* `working/extractions-hist.csv`, per-trial data with the above values tabulated from historical data

## `3_feasibility_location.R`

Calculates the total goal enrolment on a per-state basis

Output:

* `output/feasibility_by_state.csv`, per-state target enrolments and case counts by 2020-06-30
* `plots/feasibility_by_state.pdf`, a linear model for number of trial locations in a state that are in a non-feasible trial vs proportion of cases required to reach enrolment goal
* `plots/feasibility_by_state.png`, a linear model for number of trial locations in a state that are in a non-feasible trial vs proportion of cases required to reach enrolment goal

## `4_quality.R`

Calculates measures of trial quality

Output:

* `output/quality.csv`, per-trial quality data for phase 2/3 and higher

## `5_progress.R`

Calculates trial progress

## `6_redundancy.R`

Calculates per-trial measures of redundancy

Output:

* `working/potential-dup-trials.csv`, a listing of whether a trial is a potential duplicate based on all the pre-specified criteria except the per-arm ones

## `7_duplicates.R`

Provides instructions for de-duplicating drug names from trial arms, combining manual and automatic techniques

Output:

* `working/drug_names.csv`, per-arm data with final de-duplicated drug names
* `working/arms_dup_check.csv`, a list of arms to check for duplications

## `8_clinicaltrial_dl.R`

Distinguishes single- vs multi-centre trials, identifies lead agency sponsor class and determines whether a trial has a site in the USA.

Output:

* `working/clinicaltrial_dl.csv`, per trial data: single- vs multi-centre, lead agency sponsor class, location country
